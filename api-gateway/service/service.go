package service

import (
	"api-gateway/entity"
	taskpb "api-gateway/protos"
	"context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type Service struct {
	Service TaskServiceClient
}
type TaskServiceClient interface {
	CreateTask(ctx context.Context, req entity.CreateTaskRequest) (entity.TaskResponse, error)
}

func New(bookServiceUrl string) Service {
	conn, err := grpc.Dial(bookServiceUrl, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		panic(err)
	}
	return Service{
		Service: taskServiceAdapter{
			client: taskpb.NewTaskServiceClient(conn),
		},
	}
}

type taskServiceAdapter struct {
	client taskpb.TaskServiceClient
}

func (t taskServiceAdapter) CreateTask(ctx context.Context, req entity.CreateTaskRequest) (entity.TaskResponse, error) {
	grpcRequest := &taskpb.CreateTaskRequest{
		SubName:  req.SubName,
		TaskName: req.TaskName,
		Deadline: req.Deadline,
	}
	response, err := t.client.CreateTask(ctx, grpcRequest)
	if err != nil {
		return entity.TaskResponse{}, err
	}
	return entity.TaskResponse{
		Id:       response.Id,
		SubName:  response.SubName,
		TaskName: response.TaskName,
		Deadline: response.Deadline,
		Done:     false,
		DoneTime: response.DoneTime,
	}, nil
}
