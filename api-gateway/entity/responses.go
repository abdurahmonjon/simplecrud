package entity

type TaskResponse struct {
	Id       string `json:"id"`
	SubName  string `json:"sub_name"`
	TaskName string `json:"task_name"`
	Deadline string `json:"deadline"`
	Done     bool   `json:"done"`
	DoneTime string `json:"done_time"`
}
