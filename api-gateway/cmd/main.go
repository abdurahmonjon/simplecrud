package main

import (
	"api-gateway/handler"
	"api-gateway/service"
	"github.com/gin-gonic/gin"
)

func main() {
	h := handler.New(service.New("localhost:8080"))

	r := gin.Default()

	r.POST("/task", h.CreateTask)

}

