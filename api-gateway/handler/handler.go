package handler

import (
	"api-gateway/entity"
	"api-gateway/service"
	"github.com/gin-gonic/gin"
)

type Handler struct {
	service service.Service
}

func New(service service.Service) Handler {
	return Handler{service: service}
}

// CreateTask
// @Summary      Task
// @Description
// @Tags         task
// @Accept       json
// @Produce      json
// @Param        request body entity.CreateTaskRequest true "CreateTask"
// @Success      200 {object} entity.TaskResponse
// @Failure      400
// @Failure      500
// @Router       /book [POST]
func (h Handler) CreateTask(c *gin.Context) {
	var request entity.CreateTaskRequest
	err := c.ShouldBindJSON(&request);
	if err != nil {
		c.JSON(400, gin.H{"ERROR": err.Error()})
		return
	}

}
